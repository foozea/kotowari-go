Kotowari : a software for the game of Go
========================================

![Kotowari logo](http://kotowari.io/img/kotowari-logo.jpg)

## Overview

Kotowari is a software for the game of Go written in Golang.
It currently uses the Monte-Carlo tree search that uses UCT.

*It is still weak.*


## Build status (drone.io)

[![Build Status](https://drone.io/bitbucket.org/foozea/kotowari/status.png)](https://drone.io/bitbucket.org/foozea/kotowari/latest)

### GTP Support

Currently, following commands are implemented:

* protocol_version
* name
* version
* list_commands
* komi
* play
* genmove
* quit
* known_command
* boardsize
* clear_board
* showboard

## Building yourself

    go get code.kotowari.io/kotowari
    go install code.kotowari.io/kotowari

## Requirements

* The [Go](http://golang.org) Programming Language.
* [Ginkgo](http://onsi.github.io/ginkgo/) and [Gomega](http://onsi.github.io/gomega/) for testing.

## Terms of use

Kotowari is free, and distributed under the **GNU General Public License**
(GPL). Essentially, this means that you are free to do almost exactly
what you want with the program, including distributing it among your
friends, making it available for download from your web site, selling
it (either by itself or as part of some bigger software package), or
using it as the starting point for a software project of your own.

The only real limitation is that whenever you distribute Stockfish in
some way, you must always include the full source code, or a pointer
to where the source code can be found. If you make any changes to the
source code, these changes must also be made available under the GPL.

For full details, read the copy of the GPL found in the file named
*LICENSE*
