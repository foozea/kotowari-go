package stone_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestStone(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Stone Suite")
}
