package stone_test

import (
	. "code.kotowari.io/kotowari/board/stone"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "math/rand"
	. "time"
)

func init() {
	Seed(Now().UTC().UnixNano())
}

var _ = Describe("Stone", func() {
	It("Opposite", func() {
		Expect(Empty.Opposite()).To(Equal(Empty))
		Expect(Wall.Opposite()).To(Equal(Empty))
		Expect(Black.Opposite()).To(Equal(White))
		Expect(White.Opposite()).To(Equal(Black))
	})

	It("StringToStone", func() {
		Expect(StringToStone("black")).To(Equal(Black))
		Expect(StringToStone("white")).To(Equal(White))
		Expect(StringToStone("resign")).To(Equal(Empty))
		Expect(StringToStone("pass")).To(Equal(Wall))
	})

	It("Dump", func() {
		Black.Dump()
		White.Dump()
		Empty.Dump()
	})
})
