/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package board

import (
	. "code.kotowari.io/kotowari/board/bitboard"
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"

	"fmt"
)

// Represents Go GoBoard.
type GoBoard struct {
	blacks Bitboard
	whites Bitboard
	Size   BoardSize
}

// Creates new GoBoard with given board-size.
func CreateGoBoard(size BoardSize) GoBoard {
	n := size.Capacity()
	return GoBoard{CreateBitboard(n), CreateBitboard(n), size}
}

// Gets the stone at given vertex.
// If the vertex is invalid, returns Wall.
func (brd *GoBoard) GetStone(vx Vertex) Stone {
	if !vx.IsValid(brd.Size) {
		return Wall
	}
	n := vx.ToPlainIndex(brd.Size)
	if brd.blacks.GetBit(n) == 1 {
		return Black
	} else if brd.whites.GetBit(n) == 1 {
		return White
	} else {
		return Empty
	}
}

// Sets the stone at given vertex.
// If the vertex is invalid, return false.
func (brd *GoBoard) SetStone(stone Stone, vx Vertex) bool {
	if !vx.IsValid(brd.Size) || stone == Wall {
		return false
	}
	n := vx.ToPlainIndex(brd.Size)
	if stone == Black {
		brd.blacks.SetBit(n)
	} else if stone == White {
		brd.whites.SetBit(n)
	} else {
		brd.blacks.ClearBit(n)
		brd.whites.ClearBit(n)
	}
	return true
}

// If the stone at given vertex is same color as given stone, take it.
// And if neighbour stones are same color, take them. (recursively)
// Finally, this function returns total count of prisoners.
//
// This function must be called after clarified that the vertex has
// no neutral vertex.
func (brd *GoBoard) TakeStone(stone Stone, vx Vertex) int {
	checkmap := make([]bool, brd.Size.Capacity())
	return brd.takeStoneRec(stone, vx, checkmap)
}

// Main Logic for the function <TakeStone(...)>.
// Private
func (brd *GoBoard) takeStoneRec(stone Stone, vx Vertex, checkmap []bool) int {
	// 1. If the stone at given vertex is not same as given one, return 0.
	picked := brd.GetStone(vx)
	if picked != stone {
		return 0
	}
	// 2. If the vertex is already checked, returns previous result.
	index := vx.ToPlainIndex(brd.Size)
	if checkmap[index] {
		// already counted
		return 0
	}
	// 3. If not the above, check the neighbour vertexs recursively.
	brd.SetStone(Empty, vx)
	prisoners := 1
	prisoners += brd.takeStoneRec(stone, vx.Left(), checkmap)
	prisoners += brd.takeStoneRec(stone, vx.Right(), checkmap)
	prisoners += brd.takeStoneRec(stone, vx.Up(), checkmap)
	prisoners += brd.takeStoneRec(stone, vx.Down(), checkmap)
	checkmap[index] = true
	// 4. returns the total count of prisoners.
	return prisoners
}

// Returns a slice of all the stones.
func (brd *GoBoard) AllStones() []Stone {
	stones := make([]Stone, brd.Size.Capacity())
	for i := 0; i < brd.Size.Capacity(); i++ {
		stones[i] = brd.GetStone(ToVertex(i, brd.Size))
	}
	return stones
}

// Returns a slice of vertexes these are not occupied.
func (brd *GoBoard) Empties() []Vertex {
	vs := make([]Vertex, 0)
	bits := Or(brd.blacks, brd.whites)
	for i := 0; i < brd.Size.Capacity(); i++ {
		v := ToVertex(i, brd.Size)
		if bits.GetBit(i) == 0 {
			vs = append(vs, v)
		}
	}
	return vs
}

// Counts stones and eyes.
// the value is positive : black dominates
// the value is negative : white dominates
// *) Komi is not included
func (brd *GoBoard) Score() int {
	score := brd.blacks.CountBit() - brd.whites.CountBit()
	for _, v := range brd.Empties() {
		up, down, left, right :=
			v.Up().ToPlainIndex(brd.Size), v.Down().ToPlainIndex(brd.Size),
			v.Left().ToPlainIndex(brd.Size), v.Right().ToPlainIndex(brd.Size)
		bn := brd.blacks.GetBit(up) + brd.blacks.GetBit(down) + brd.blacks.GetBit(left) + brd.blacks.GetBit(right)
		wn := brd.whites.GetBit(up) + brd.whites.GetBit(down) + brd.whites.GetBit(left) + brd.whites.GetBit(right)
		if bn != 0 && wn == 0 {
			score++
		} else if bn == 0 && wn != 0 {
			score--
		}
	}
	return score
}

// Copies the GoBoard and returns it.
func (brd *GoBoard) Copy() GoBoard {
	copied := CreateGoBoard(brd.Size)
	copy(copied.blacks, brd.blacks)
	copy(copied.whites, brd.whites)
	return copied
}

// Displays the state of the board to console.
func (brd *GoBoard) Dump() {
	ls := brd.Size.LineSize()
	stones := brd.AllStones()
	files := "ABCDEFGHJKLMNOPQRSTUVWXYZ"

	// Header
	fmt.Printf("\n")
	for i := 0; i < ls; i++ {
		fmt.Printf("%v ", string(files[i]))
	}
	fmt.Printf("\n")
	// Body
	for i := 0; i < ls; i++ {
		n := len(stones)
		sts := stones[n-ls*(i+1) : n-ls*i]
		for j, v := range sts {
			v.Dump()
			fmt.Printf(" ")
			if (j+1)%ls == 0 {
				fmt.Printf(" %v\n", ls-i)
			}
		}
	}
	fmt.Printf("\n")
}
