/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package bitboard

// bitboard structure
type Bitboard []uint64

func CreateBitboard(size int) Bitboard {
	ssize := size/64 + 1 // slice size
	return make([]uint64, ssize)
}

// Gets a int value with given index.
func (b *Bitboard) GetBit(index int) int {
	sidx := index / 64 // slice index
	bidx := index % 64 // bit index
	return int((*b)[sidx]>>uint(bidx)) & 0x1
}

// Gets a bit with given index.
func (b *Bitboard) SetBit(index int) {
	sidx := index / 64 // slice index
	bidx := index % 64 // bit index
	(*b)[sidx] |= uint64(0x1 << uint(bidx))
}

// Gets a bit with given index.
func (b *Bitboard) ClearBit(index int) {
	sidx := index / 64 // slice index
	bidx := index % 64 // bit index
	(*b)[sidx] &= ^(0x1 << uint(bidx))
}

func (b *Bitboard) BitSize() int {
	return len(*b) * 64
}

func (b *Bitboard) Copy() Bitboard {
	calc := CreateBitboard(b.BitSize())
	copy(calc, (*b))
	return calc
}

func (b *Bitboard) CountBit() int {
	ret := 0
	for _, v := range *b {
		n := v
		for n > 0 {
			n &= n - 1
			ret++
		}
	}
	return ret
}

func And(lhs Bitboard, rhs Bitboard) Bitboard {
	calc := CreateBitboard(lhs.BitSize())
	for i, v := range lhs {
		calc[i] = v & rhs[i]
	}
	return calc
}

func Or(lhs Bitboard, rhs Bitboard) Bitboard {
	calc := CreateBitboard(lhs.BitSize())
	for i, v := range lhs {
		calc[i] = v | rhs[i]
	}
	return calc
}

func Xor(lhs Bitboard, rhs Bitboard) Bitboard {
	calc := CreateBitboard(lhs.BitSize())
	for i, v := range lhs {
		calc[i] = v ^ rhs[i]
	}
	return calc
}

func Not(arg Bitboard) Bitboard {
	calc := CreateBitboard(arg.BitSize())
	for i, v := range arg {
		calc[i] = ^v
	}
	return calc
}
