package bitboard_test

import (
	. "code.kotowari.io/kotowari/board/bitboard"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Bitboard", func() {
	It("SetBit/GetBit/ClearBit", func() {
		bits := CreateBitboard(100)
		bits.SetBit(10)
		Expect(bits[0]).To(Equal(uint64(1024)))
		Expect(bits.GetBit(10)).To(Equal(1))
		bits.ClearBit(10)
		Expect(bits.GetBit(10)).To(Equal(0))
		bits.SetBit(74)
		Expect(bits[1]).To(Equal(uint64(1024)))
		Expect(bits.GetBit(74)).To(Equal(1))
		bits.ClearBit(74)
		Expect(bits.GetBit(74)).To(Equal(0))
	})

	It("CountBit", func() {
		bits := CreateBitboard(100)
		bits.SetBit(10)
		Expect(bits.CountBit()).To(Equal(1))
		bits.SetBit(15)
		Expect(bits.CountBit()).To(Equal(2))
	})
})
