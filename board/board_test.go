package board_test

import (
	. "code.kotowari.io/kotowari/board"
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "math/rand"
	. "time"
)

func init() {
	Seed(Now().UTC().UnixNano())
}

var _ = Describe("Board", func() {
	var (
		size   BoardSize
		bIndex int
		wIndex int
		board  GoBoard
		copied GoBoard
	)

	BeforeEach(func() {
		size = B19x19
		bIndex = Intn(size.Capacity())
		wIndex = Intn(size.Capacity())
		for bIndex == wIndex {
			wIndex = Intn(size.Capacity())
		}
		board = CreateGoBoard(size)
	})

	It("CreateGoBoard", func() {
		newb := CreateGoBoard(size)
		for i := 0; i < size.Capacity(); i++ {
			if i != bIndex && i != wIndex {
				Expect(newb.GetStone(ToVertex(i, size))).To(Equal(Empty))
			}
		}
	})

	It("Get/SetStone", func() {
		board.SetStone(Black, ToVertex(bIndex, size))
		board.SetStone(White, ToVertex(wIndex, size))
		Expect(board.GetStone(ToVertex(bIndex, size))).To(Equal(Black))
		Expect(board.GetStone(ToVertex(wIndex, size))).To(Equal(White))
		Expect(board.SetStone(White, Outbound())).NotTo(BeTrue())
		Expect(board.GetStone(Outbound())).To(Equal(Wall))
	})

	It("Empties", func() {
		occ := CreateGoBoard(size)
		occ.SetStone(Black, ToVertex(bIndex, size))
		occ.SetStone(White, ToVertex(wIndex, size))
		vxs := occ.Empties()
		Expect(len(vxs)).To(Equal(size.Capacity() - 2))
	})

	It("Copy", func() {
		board.SetStone(Black, ToVertex(bIndex, size))
		board.SetStone(White, ToVertex(wIndex, size))
		copied = board.Copy()
		for i := 0; i < size.Capacity(); i++ {
			p := ToVertex(i, size)
			Expect(copied.GetStone(p)).To(Equal(board.GetStone(p)))
		}
		copied.SetStone(Black, ToVertex(wIndex, size))
		Expect(copied.GetStone(ToVertex(wIndex, size))).NotTo(Equal(White))
	})

	It("TakeStone", func() {
		taken := CreateGoBoard(size)
		vx := ToVertex(Intn(size.Capacity()), size)

		taken.SetStone(Black, vx)
		count := 1
		if taken.SetStone(Black, vx.Up()) {
			count++
		}
		if taken.SetStone(Black, vx.Down()) {
			count++
		}
		if taken.SetStone(Black, vx.Left()) {
			count++
		}
		if taken.SetStone(Black, vx.Right()) {
			count++
		}

		// Not taken
		taken.SetStone(Black, vx.Right().Right().Right())

		Expect(taken.TakeStone(Black, vx)).To(Equal(count))

		taken.Dump()
	})
})
