package size_test

import (
	. "code.kotowari.io/kotowari/board/size"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "math/rand"
	. "time"
)

func init() {
	Seed(Now().UTC().UnixNano())
}

var _ = Describe("BoardSize", func() {
	It("Capacity", func() {
		Expect(B9x9.Capacity()).To(Equal(81))
		Expect(B11x11.Capacity()).To(Equal(121))
		Expect(B13x13.Capacity()).To(Equal(169))
		Expect(B15x15.Capacity()).To(Equal(225))
		Expect(B19x19.Capacity()).To(Equal(361))
		Expect(BoardSize(9999).Capacity()).To(Equal(0))
	})

	It("LineSize", func() {
		Expect(B9x9.LineSize()).To(Equal(9))
		Expect(B11x11.LineSize()).To(Equal(11))
		Expect(B13x13.LineSize()).To(Equal(13))
		Expect(B15x15.LineSize()).To(Equal(15))
		Expect(B19x19.LineSize()).To(Equal(19))
	})
})
