package size_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestSize(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Size Suite")
}
