/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package vertex

import (
	. "code.kotowari.io/kotowari/board/size"
	"strconv"
	"strings"
)

// Represents vertex.
// file : x-coordinate
// rank : y-coordinate.
type Vertex struct {
	File int
	Rank int
}

var outbound = Vertex{-1, -1}

// Returns the invalid vertex.
// This is used to show the vertex is not exists.
func Outbound() Vertex {
	return outbound
}

// Transforms simple integer index to Vertex structure.
// The file and rank depend on the given board-size.
func ToVertex(index int, size BoardSize) Vertex {
	n := size.LineSize()
	return Vertex{index % n, index / n}
}

// Transforms Vertex structure to simple integer index.
func (vx Vertex) ToPlainIndex(size BoardSize) int {
	return vx.Rank*size.LineSize() + vx.File
}

// Determines if the Vertex is valid as the coordinate on the Go board.
func (vx Vertex) IsValid(size BoardSize) bool {
	n := size.LineSize()
	return (n > vx.File && vx.File >= 0) && (n > vx.Rank && vx.Rank >= 0)
}

// Gets upper vertex.
func (vx Vertex) Up() Vertex {
	return Vertex{vx.File, vx.Rank + 1}
}

// Gets lower vertex.
func (vx Vertex) Down() Vertex {
	return Vertex{vx.File, vx.Rank - 1}
}

// Gets left vertex.
func (vx Vertex) Left() Vertex {
	return Vertex{vx.File + 1, vx.Rank}
}

// Gets right vertex.
func (vx Vertex) Right() Vertex {
	return Vertex{vx.File - 1, vx.Rank}
}

func (vx Vertex) ToString() string {
	if vx != Outbound() {
		f := string('A' + vx.File)
		if vx.File > 8 {
			f = string('A' + vx.File + 1)
		}
		r := strconv.Itoa(vx.Rank + 1)
		return f + r
	} else {
		return "PASS"
	}
}

// Parse string and returns a vertex.
func StringToVertex(str string) Vertex {
	str = strings.ToUpper(str)
	if len(str) < 2 || str == "PASS" {
		return Outbound()
	}
	file := int(([]rune(str[:1]))[0] - 'A')
	if file > 8 {
		file -= 1
	}
	rank, err := strconv.Atoi(str[1:len(str)])
	if err == nil {
		return Vertex{file, rank - 1}
	}
	return Outbound()
}
