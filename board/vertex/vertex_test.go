package vertex_test

import (
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/vertex"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "math/rand"
	. "time"
)

func init() {
	Seed(Now().UTC().UnixNano())
}

var _ = Describe("Vertex", func() {
	var (
		vertexIndex  Vertex
		integerIndex int
		regenIndex   int
	)

	BeforeEach(func() {
		integerIndex = Intn(B19x19.Capacity())
		vertexIndex = ToVertex(integerIndex, B19x19)
		regenIndex = vertexIndex.ToPlainIndex(B19x19)
	})

	It("Outbound", func() {
		Expect(Outbound()).To(Equal(Vertex{-1, -1}))
	})

	It("ToVertex", func() {
		Expect(vertexIndex.File).To(Equal(integerIndex % B19x19.LineSize()))
		Expect(vertexIndex.Rank).To(Equal(integerIndex / B19x19.LineSize()))
	})

	It("ToPlainIntex", func() {
		Expect(regenIndex).To(Equal(integerIndex))
	})

	It("Move functions", func() {
		Expect(vertexIndex.Left()).To(Equal(Vertex{vertexIndex.File + 1, vertexIndex.Rank}))
		Expect(vertexIndex.Right()).To(Equal(Vertex{vertexIndex.File - 1, vertexIndex.Rank}))
		Expect(vertexIndex.Up()).To(Equal(Vertex{vertexIndex.File, vertexIndex.Rank + 1}))
		Expect(vertexIndex.Down()).To(Equal(Vertex{vertexIndex.File, vertexIndex.Rank - 1}))
	})

	It("IsValid", func() {
		p := Vertex{0, 1}
		Expect(p.IsValid(B9x9)).To(Equal(true))
		p = Vertex{9, 1}
		Expect(p.IsValid(B9x9)).To(Equal(false))
		p = Vertex{1, 0}
		Expect(p.IsValid(B9x9)).To(Equal(true))
		p = Vertex{1, 9}
		Expect(p.IsValid(B9x9)).To(Equal(false))
	})

	It("StringToVertex", func() {
		Expect(StringToVertex("X")).To(Equal(Outbound()))
		Expect(StringToVertex("XXXX")).To(Equal(Outbound()))
		Expect(StringToVertex("XYZ")).To(Equal(Outbound()))
		Expect(StringToVertex("D4")).To(Equal(Vertex{3, 3}))
		Expect(StringToVertex("L18")).To(Equal(Vertex{10, 17}))
	})

	It("ToString", func() {
		Expect(StringToVertex("D4").ToString()).To(Equal("D4"))
		Expect(StringToVertex("L18").ToString()).To(Equal("L18"))
		Expect(StringToVertex("X").ToString()).To(Equal("PASS"))
	})
})
