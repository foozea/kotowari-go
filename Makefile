## Kotowari, a software for the game of Go
## Copyright (C) 2014 Tetsuo FUJII
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

# The name of the executable
BINNAME=kotowari

# Gets package list
PACKS=$(sort $(filter-out ./, $(dir $(shell find . -not -path "*.git*"))))
PROFS=$(shell find . -iname "*.coverprofile")

# Standard, run
.PHONY: default run

# 'go run' after 'go fmt'
run:format
	CGO_ENABLED=0 go run $(BINNAME).go

# Do the tests using Ginkgo
test:
	@echo "Testing ..."
	ginkgo -r --randomizeAllSpecs -cover

# 'go build' after the tests.
build:test
	@echo "Building ..."
	go build $(BINNAME).go

# 'go fmt' for all the go-code.
format:
	@echo "Formatting ..."
	@LIST="$(PACKS)";\
	for x in $$LIST; do\
		go fmt $$x/*.go;\
	done
	@go fmt $(BINNAME).go

# 'go install'
install:build
	@echo "Installing ..."
	go install

# Clean all
clean:
	@echo "Deleting ${BINNAME} ..."
	@LIST="$(PACKS)";\
	for x in $$LIST; do\
		rm $$x/*.coverprofile;\
		rm $$x/*.exe;\
	done
	@rm $(BINNAME).exe
