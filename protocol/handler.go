/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package protocol

import (
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	. "code.kotowari.io/kotowari/rule/position"

	"fmt"
	"os"
	"strconv"
)

type Args []string

// Clears the arguments.
func (a *Args) Clear() {
	*a = make(Args, 2)
}

// Common Interface
type Handler func(Args)

// Functions (Handlers) in this package are used for each command these have
// the same name as handlers.

// Prints "protocol version" that is defined at protocol.go
// and set at the start point of the program.
func protocol_version(args Args) {
	fmt.Printf("= %v\n\n", PROTOCOL_VERSION)
}

// Prints "engine name" that is defined at protocol.go
// and set at the start point of the program.
func name(args Args) {
	fmt.Printf("= %v\n\n", Engine.Name)
}

// Prints "engine version"
func version(args Args) {
	fmt.Printf("= %v\n\n", Engine.Version)
}

// If the given command-name is implemented at this program, returns true.
func known_command(args Args) {
	fmt.Print("= ")
	if len(args) == 0 {
		fmt.Println("false\n")
	} else {
		fmt.Printf("%v\n\n", Dispatcher.HasHandler(args[0]))
	}
}

// List ups all the commands these are implemented at this program.
func list_commands(args Args) {
	fmt.Print("= ")
	for k, _ := range Dispatcher {
		fmt.Println(k)
	}
	fmt.Printf("\n")
}

// Quits program with exit code 0.
func quit(args Args) {
	os.Exit(0)
}

// Sets board-size with integer.
// After that, clear_board command is processed.
func boardsize(args Args) {
	if len(args) == 0 {
		fmt.Println("? boardsize must be an integer\n")
		return
	}
	v, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("? boardsize must be an integer\n")
		return
	}
	switch v {
	case 9:
		GameController.Size = B9x9
	case 11:
		GameController.Size = B11x11
	case 13:
		GameController.Size = B13x13
	case 15:
		GameController.Size = B15x15
	case 19:
		GameController.Size = B19x19
	default:
		fmt.Println("? unacceptable size\n")
		return
	}
	clear_board(args)
}

// Clears the board.
func clear_board(args Args) {
	GameController.ClearHistory()
	fmt.Println("=\n")
}

// Sets Komi value.
func komi(args Args) {
	if len(args) == 0 {
		fmt.Println("? komi must be a float\n")
		return
	}
	v, err := strconv.ParseFloat(args[0], 64)
	if err != nil {
		fmt.Println("? komi must be a float\n")
		return
	}
	GameController.Komi = v
	Engine.Komi = v
	fmt.Println("=\n")
}

// Plays a move. after this function, game end check must be done.
func play(args Args) {
	if len(args) < 2 {
		fmt.Println("? invalid parameter(s)\n")
		return
	}
	// 1st argument is treated as stone
	stone := StringToStone(args[0])
	if stone == Wall || stone == Empty {
		fmt.Println("? invalid parameter(s)\n")
		return
	}
	// 2nd argument is treated as coordinate
	point := StringToVertex(args[1])
	if point == Outbound() { //pass
		GameController.Pass()
		fmt.Println("=\n")
		return
	}
	ok := GameController.MakeMove(CreateMove(stone, point))
	if !ok {
		fmt.Println("? illegal move\n")
		return
	}
	fmt.Println("=\n")
}

// Generates a move.
func genmove(args Args) {
	if len(args) == 0 {
		fmt.Println("? invalid parameter(s)\n")
		return
	}
	// 1st argument is treated as stone
	stone := StringToStone(args[0])
	if stone == Wall {
		fmt.Println("? invalid parameter(s)\n")
		return
	}
	if stone == Empty { // resign
		fmt.Println("= resign\n")
		return
	}
	pos := GameController.GetCurrentPosition()
	mv := Engine.Ponder(pos, stone)
	_ = GameController.MakeMove(mv)
	fmt.Printf("= %v\n\n", mv.ToString())
}

// Show board for use of debugging.
func showboard(args Args) {
	fmt.Println("=\n")
	pos := GameController.GetCurrentPosition()
	prison := pos.Prison
	pos.Dump()
	fmt.Printf("Black (X) : %v stones\n", prison.Black)
	fmt.Printf("White (O) : %v stones\n\n", prison.White)
}
