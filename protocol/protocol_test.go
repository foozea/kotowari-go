package protocol_test

import (
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/protocol"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Protocol", func() {

	It("Initialize", func() {
		Expect(Dispatcher.HasHandler(Protocol_version.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Name.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Known_command.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(List_commands.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Boardsize.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Clear_board.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Komi.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Play.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Genmove.ToString())).To(BeTrue())
		Expect(Dispatcher.HasHandler(Showboard.ToString())).To(BeTrue())
	})

	It("protocol_version", func() {
		ArgsForHandlers.Clear()
		Dispatcher.CallHandler(Protocol_version.ToString())
	})

	It("name", func() {
		ArgsForHandlers.Clear()
		Dispatcher.CallHandler(Name.ToString())
	})

	It("known_command", func() {
		ArgsForHandlers.Clear()
		ArgsForHandlers[0] = "protocol_version"
		Dispatcher.CallHandler(Known_command.ToString())
		ArgsForHandlers.Clear()
		ArgsForHandlers[0] = "foobar"
		Dispatcher.CallHandler(Known_command.ToString())
		ArgsForHandlers.Clear()
		Dispatcher.CallHandler(Known_command.ToString())
	})

	It("list_commands", func() {
		ArgsForHandlers.Clear()
		Dispatcher.CallHandler(List_commands.ToString())
	})

	It("boardsize", func() {
		ArgsForHandlers.Clear()
		ArgsForHandlers[0] = "9"
		Dispatcher.CallHandler(Boardsize.ToString())
		Expect(GameController.Size).To(Equal(B9x9))
	})

	It("clear_board", func() {
		ArgsForHandlers.Clear()
		ArgsForHandlers[0] = "9"
		Dispatcher.CallHandler(Clear_board.ToString())
		Expect(GameController.CurrentStoneIs()).To(Equal(Empty))
	})

	It("showboard", func() {
		ArgsForHandlers.Clear()
		Dispatcher.CallHandler(Showboard.ToString())
	})
})
