/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package engine

import (
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	. "code.kotowari.io/kotowari/misc"
	. "code.kotowari.io/kotowari/rule/position"

	. "math"
	. "math/rand"
	. "time"

	"log"
)

func init() {
	Seed(Now().UTC().UnixNano())
}

type Kotowari struct {
	Name    string
	Version string
	Komi    float64
	Trials  int
}

func CreateEngine(name string, version string) Kotowari {
	return Kotowari{name, version, 0.0, 10000}
}

func (n *Kotowari) Ponder(pos *Position, s Stone) Move {
	defer Un(Trace("Kotowari#Ponder"))
	for i := 0; i < n.Trials; i++ {
		n.UCT(pos, s)
	}
	selected := CreateMove(Empty, Outbound())
	max := -999.0
	for _, v := range pos.Moves {
		log.Printf("%s: %f(%f)\n", v.ToString(), v.Rate, v.Games)
		if v.Games > max {
			selected = v
			max = v.Games
		}
	}
	log.Printf("playout: %v cycle\n", n.Trials)
	return selected
}

func (n *Kotowari) UCT(current *Position, s Stone) float64 {
	pos := current
	maxUcb := -999.0
	selected := 0
	// If moves-slice is empty, create all moves.
	if len(pos.Moves) == 0 {
		cs := pos.Empties()
		for _, v := range cs {
			mv := CreateMove(s, v)
			pos.Moves = append(pos.Moves, mv)
		}
		// Pass
		pos.Moves = append(pos.Moves, CreateMove(Empty, Outbound()))
	}
	for i, v := range pos.Moves {
		if !pos.IsLegalMove(v) || pos.IsFillEyeMove(v) {
			// if the move is illegal, skip it.
			continue
		}
		ucb := 0.0
		if v.Games == 0 {
			ucb = 10000 + Float64()
		} else {
			const C = 0.31
			ucb = v.Rate + C*Sqrt(Log10(pos.Games)/v.Games)
		}
		if ucb > maxUcb {
			maxUcb = ucb
			selected = i
		}
	}
	next := pos.MakeMove(pos.Moves[selected])
	pos.Moves[selected].Next = next
	win := 0.0
	if pos.Moves[selected].Games == 0.0 {
		win -= n.playout(&next, s.Opposite())
	} else {
		win -= n.UCT(&next, s.Opposite())
	}
	pos.Moves[selected].Rate =
		(pos.Moves[selected].Rate*pos.Moves[selected].Games + win) /
			(pos.Moves[selected].Games + 1)
	pos.Moves[selected].Games++
	pos.Games++
	return win
}

func (n *Kotowari) playout(current *Position, stone Stone) float64 {
	pos := current.Copy()
	maxDepth := pos.GetSize().Capacity() + 200
	s := stone
	depth := 1
	passed := false
	rest := pos.Empties()

	for depth < maxDepth {
		var m Move
		m, rest = n.Inspiration(&pos, rest, s)
		if m.Vertex == Outbound() {
			if passed {
				break
			} else {
				passed = true
			}
		} else {
			passed = false
			pos = pos.MakeMove(m)
		}
		s = s.Opposite()
		depth++
	}
	return pos.Score(n.Komi, stone)
}

func (n *Kotowari) Inspiration(pos *Position, current []Vertex, s Stone) (move Move, rest []Vertex) {
	// preparation
	max, loop := len(current), make([]Vertex, len(current))
	copy(loop, current)
	// loop at all candidates randomly
	for i := 0; i < len(loop); i++ {
		// shuffle
		j := Intn(max)
		loop[i], loop[j] = loop[j], loop[i]
		// if the move is valid, returns it.
		mv := CreateMove(s, loop[i])
		if pos.IsLegalMove(mv) && !pos.IsFillEyeMove(mv) {
			return mv, append(loop[:i], loop[i+1:]...)
		}
	}
	return CreateMove(Empty, Outbound()), loop
}
