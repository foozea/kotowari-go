package prison_test

import (
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/rule/prison"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Prison", func() {
	It("Add", func() {
		prisoners := Prisoners{0, 0}
		prisoners.Add(Black, 1)
		Expect(prisoners.Black).To(Equal(1))
	})

	It("Copy", func() {
		prisoners := Prisoners{0, 0}
		copied := prisoners.Copy()
		copied.Add(White, 3)
		copied.Add(Black, 10)
		Expect(copied).NotTo(Equal(prisoners))
	})
})
