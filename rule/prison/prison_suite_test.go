package prison_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestPrison(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Prison Suite")
}
