package rule_test

import (
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	. "code.kotowari.io/kotowari/rule"
	. "code.kotowari.io/kotowari/rule/position"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Rule", func() {
	var (
		size  BoardSize
		state GameState
	)

	BeforeEach(func() {
		size = B19x19
		state = CreateDefaultGameState()
	})

	It("Default", func() {
		Expect(state.Size).To(Equal(B9x9))
		Expect(state.Komi).To(Equal(0.0))
		Expect(state.Times).To(Equal(TimeSettings{60, 600, 25}))
	})

	It("MakeMove/CurrentTurnIs/ClearHistory", func() {
		ns := CreateDefaultGameState()
		Expect(ns.CurrentStoneIs()).To(Equal(Empty))
		ok := ns.MakeMove(CreateMove(Black, Vertex{1, 1}))
		Expect(ok).To(BeTrue())
		Expect(ns.CurrentStoneIs()).To(Equal(Black))
		ok = ns.MakeMove(CreateMove(White, Vertex{2, 2}))
		Expect(ok).To(BeTrue())
		Expect(ns.CurrentStoneIs()).To(Equal(White))
		ns.ClearHistory()
		Expect(ns.CurrentStoneIs()).To(Equal(Empty))
	})

	It("Pass", func() {
		ns := CreateDefaultGameState()
		ns.Pass()
	})
})
