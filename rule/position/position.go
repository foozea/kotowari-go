/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package position

import (
	. "code.kotowari.io/kotowari/board"
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	"code.kotowari.io/kotowari/rule/ko"
	. "code.kotowari.io/kotowari/rule/prison"
)

// Represents a position of a go game.
type Position struct {
	board  GoBoard
	Ko     ko.Ko
	Prison Prisoners
	Games  float64
	Moves  []Move
}

// Creates new position.
func CreatePosition(size BoardSize) Position {
	return Position{CreateGoBoard(size),
		ko.Ko{Outbound(), Empty},
		Prisoners{0, 0},
		0,
		make([]Move, 0)}
}

// Determines if the move is legal or not.
func (pos *Position) IsLegalMove(move Move) bool {
	// 1. if it is Ko threat or the vertex is not empty, return false
	if (move.Stone == pos.Ko.OccuredBy.Opposite() && move.Vertex == pos.Ko.Vertex) ||
		pos.board.GetStone(move.Vertex) != Empty {
		return false
	}
	// 2. if the move is suicide, returns false.
	return !pos.IsSuicideMove(move)
}

// Determines if the move filling own Eye or not.
func (pos *Position) IsFillEyeMove(move Move) bool {
	brd := pos.board
	vx := move.Vertex
	st := move.Stone
	if brd.GetStone(vx) == Empty &&
		(brd.GetStone(vx.Up()) == st || brd.GetStone(vx.Up()) == Wall) &&
		(brd.GetStone(vx.Down()) == st || brd.GetStone(vx.Down()) == Wall) &&
		(brd.GetStone(vx.Right()) == st || brd.GetStone(vx.Right()) == Wall) &&
		(brd.GetStone(vx.Left()) == st || brd.GetStone(vx.Left()) == Wall) {
		test := pos.MakeTestMove(move)
		if test.CountLiberty(move.Stone, move.Vertex) > 1 {
			return true
		}
	}
	return false
}

// Determines if the move is suicede or not.
func (pos *Position) IsSuicideMove(move Move) bool {
	stone := move.Stone
	vx := move.Vertex
	//   make tempolary position
	test := pos.MakeTestMove(move)
	// 1. check the tempolary position and if the stone is not dead,
	//    it is not suicide move.
	if !test.IsDead(stone, vx) {
		return false
	}
	// 2. if the move can take opponent stone(s), not suicide.
	tbrd := test.board
	opp := stone.Opposite()
	if (tbrd.GetStone(vx.Left()) == opp && test.IsDead(opp, vx.Left())) ||
		(tbrd.GetStone(vx.Right()) == opp && test.IsDead(opp, vx.Right())) ||
		(tbrd.GetStone(vx.Up()) == opp && test.IsDead(opp, vx.Up())) ||
		(tbrd.GetStone(vx.Down()) == opp && test.IsDead(opp, vx.Down())) {
		return false
	}
	// 3. else, suicide move.
	return true
}

// Determines if the given vertex has neutral vertex (dame) or not.
// This function check the given vertex first, and second, check the neighbours
// recursively.
func (pos *Position) IsDead(stone Stone, vx Vertex) bool {
	dame := pos.CountLiberty(stone, vx)
	if dame == 0 {
		return true
	}
	return false
}

// Count the number of dame. (recursively)
func (pos *Position) CountLiberty(stone Stone, vx Vertex) int {
	checkmap := make([]bool, pos.GetSize().Capacity())
	return pos.countLibRec(stone, vx, checkmap)
}

// Main Logic for the function <CountLiberty(...)>.
// Private
func (pos *Position) countLibRec(stone Stone, vx Vertex, checkmap []bool) int {
	s := pos.board.GetStone(vx)
	index := vx.ToPlainIndex(pos.GetSize())
	// 1. if the vertex is wall, return 0.
	if s == Wall {
		return 0
	}
	// 2. if already checked, return 0
	if checkmap[index] {
		return 0
	}
	checkmap[index] = true
	if s == Empty {
		// 3. if the vertex is empty, return 1.
		return 1
	} else if s == stone.Opposite() {
		// 4. if the position is opponent, return 0
		return 0
	}
	// 5. check neighbours recursively.
	return pos.countLibRec(stone, vx.Left(), checkmap) +
		pos.countLibRec(stone, vx.Right(), checkmap) +
		pos.countLibRec(stone, vx.Up(), checkmap) +
		pos.countLibRec(stone, vx.Down(), checkmap)
}

// Make test move.
func (pos *Position) MakeTestMove(move Move) Position {
	next := pos.Copy()
	next.board.SetStone(move.Stone, move.Vertex)
	return next
}

// Makes ply with given stone and vertex.
// If the vertex is {-1, -1}, treat the ply as PASS.
// Else if invalid move, returns false.
func (pos *Position) MakeMove(move Move) Position {
	next := pos.MakeTestMove(move)
	stone := move.Stone
	vx := move.Vertex
	opp := stone.Opposite()
	prisonersCount := 0
	// if the neighbours of the put stone are wall or opponent stone,
	// maybe Ko, so set flag to check later.
	ko_flag := false
	if (next.board.GetStone(vx.Left()) == opp ||
		next.board.GetStone(vx.Left()) == Wall) &&
		(next.board.GetStone(vx.Right()) == opp ||
			next.board.GetStone(vx.Right()) == Wall) &&
		(next.board.GetStone(vx.Up()) == opp ||
			next.board.GetStone(vx.Up()) == Wall) &&
		(next.board.GetStone(vx.Down()) == opp ||
			next.board.GetStone(vx.Down()) == Wall) {
		ko_flag = true
	}
	// Take stones
	if next.IsDead(opp, vx.Left()) {
		prisonersCount += next.board.TakeStone(opp, vx.Left())
	}
	if next.IsDead(opp, vx.Right()) {
		prisonersCount += next.board.TakeStone(opp, vx.Right())
	}
	if next.IsDead(opp, vx.Up()) {
		prisonersCount += next.board.TakeStone(opp, vx.Up())
	}
	if next.IsDead(opp, vx.Down()) {
		prisonersCount += next.board.TakeStone(opp, vx.Down())
	}
	if stone == Black {
		next.Prison.Black += prisonersCount
	} else {
		next.Prison.White += prisonersCount
	}
	// if captured stone number is 1, and Ko-flag is set, it is Ko.
	if prisonersCount == 1 && ko_flag {
		next.Ko.OccuredBy = stone
		if next.board.GetStone(vx.Up()) == Empty {
			next.Ko.Vertex = vx.Up()
		} else if next.board.GetStone(vx.Down()) == Empty {
			next.Ko.Vertex = vx.Down()
		} else if next.board.GetStone(vx.Left()) == Empty {
			next.Ko.Vertex = vx.Left()
		} else if next.board.GetStone(vx.Right()) == Empty {
			next.Ko.Vertex = vx.Right()
		}
	} else {
		next.Ko.Init()
	}
	return next
}

// Get candidate moves that listed randomly.
func (pos *Position) Empties() []Vertex {
	return pos.board.Empties()
}

func (pos *Position) Score(komi float64, stone Stone) float64 {
	score := float64(pos.board.Score()) - komi
	win := 0.0
	if score > 0 {
		win = 1.0
	}
	if stone == White {
		win = -win
	}
	return win
}

// Returns size of the board
func (pos *Position) GetSize() BoardSize {
	return pos.board.Size
}

// Copies the Position and returns it.
func (pos *Position) Copy() Position {
	copied := CreatePosition(pos.GetSize())
	copied.board = pos.board.Copy()
	copied.Ko = pos.Ko.Copy()
	copied.Prison = pos.Prison
	return copied
}

// Displays the state of the board to console.
func (pos *Position) Dump() {
	pos.board.Dump()
}
