package position

import (
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	. "code.kotowari.io/kotowari/rule/ko"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Position", func() {
	var (
		size         BoardSize
		center       Vertex
		initPosition Position
	)

	BeforeEach(func() {
		size = B19x19
		center = Vertex{1, 1}
		initPosition = CreatePosition(size)
	})

	It("GetSize", func() {
		Expect(initPosition.GetSize()).To(Equal(size))
	})

	It("IsLegal/IsSuicide/HasKo", func() {
		pos := CreatePosition(size)
		Expect(pos.IsLegalMove(CreateMove(Black, StringToVertex("B2")))).To(BeTrue())
		Expect(pos.IsSuicideMove(CreateMove(Black, StringToVertex("B2")))).NotTo(BeTrue())
		pos.Ko = Ko{Vertex{1, 1}, White}
		Expect(pos.IsLegalMove(CreateMove(Black, StringToVertex("B2")))).NotTo(BeTrue())

		next := pos.MakeTestMove(CreateMove(Black, center.Up()))
		next = next.MakeTestMove(CreateMove(Black, center.Down()))
		next = next.MakeTestMove(CreateMove(Black, center.Left()))
		next = next.MakeTestMove(CreateMove(Black, center.Right()))
		Expect(next.IsSuicideMove(CreateMove(White, center))).To(BeTrue())
		Expect(next.IsLegalMove(CreateMove(White, center))).NotTo(BeTrue())
		Expect(next.IsLegalMove(CreateMove(White, center.Up()))).NotTo(BeTrue())
	})

	It("IsDead", func() {
		pos := CreatePosition(size)
		Expect(pos.IsDead(Black, Outbound())).To(BeTrue())
		next := pos.MakeTestMove(CreateMove(Black, center))
		Expect(next.IsDead(White, center)).To(BeTrue())
		Expect(next.IsDead(White, center.Up())).NotTo(BeTrue())

		pos = CreatePosition(size)
		next = pos.MakeTestMove(CreateMove(Black, Vertex{0, 0}))
		next = next.MakeTestMove(CreateMove(White, Vertex{1, 0}))
		next = next.MakeTestMove(CreateMove(White, Vertex{0, 1}))
		next.Dump()
		Expect(next.IsDead(Black, Vertex{0, 0})).To(BeTrue())
	})

	It("MakeMove", func() {
		pos := CreatePosition(size)
		next := pos.MakeTestMove(CreateMove(Black, center.Up()))
		next = next.MakeTestMove(CreateMove(Black, center.Down()))
		next = next.MakeTestMove(CreateMove(Black, center.Left()))
		next = next.MakeTestMove(CreateMove(Black, center.Right()))
		nc := center.Up()
		next = next.MakeTestMove(CreateMove(White, nc.Up()))
		next = next.MakeTestMove(CreateMove(White, nc.Left()))
		next = next.MakeTestMove(CreateMove(White, nc.Right()))

		// Ko
		captured := next.MakeMove(CreateMove(White, nc.Down()))
		captured.Dump()
		Expect(captured.Prison.White).To(Equal(1))
	})

	It("Copy", func() {
		pos := CreatePosition(size)
		copied := pos.Copy()
		Expect(copied).To(Equal(pos))
		copied.board.SetStone(Black, Vertex{1, 1})
		Expect(copied).NotTo(Equal(pos))
	})

	It("Empties", func() {
		pos := CreatePosition(size)
		next := pos.MakeTestMove(CreateMove(Black, center.Up()))
		candidates := next.Empties()
		Expect(len(candidates)).To(Equal(size.Capacity() - 1))
	})

	It("CountLiberty", func() {
		pos := CreatePosition(size)
		next := pos.MakeTestMove(CreateMove(Black, StringToVertex("A2")))
		Expect(next.CountLiberty(Black, StringToVertex("A2"))).To(Equal(3))
		next = next.MakeTestMove(CreateMove(Black, StringToVertex("B1")))
		next = next.MakeTestMove(CreateMove(Black, StringToVertex("B3")))
		Expect(next.CountLiberty(Black, StringToVertex("B3"))).To(Equal(4))
		next = next.MakeTestMove(CreateMove(Black, StringToVertex("C2")))
		Expect(next.CountLiberty(Black, StringToVertex("B2"))).To(Equal(1))
		Expect(next.IsFillEyeMove(CreateMove(Black, StringToVertex("B2")))).To(BeTrue())
	})
})
