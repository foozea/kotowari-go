package ko_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestKo(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Ko Suite")
}
