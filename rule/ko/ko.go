/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ko

import (
	. "code.kotowari.io/kotowari/board/stone"
	"code.kotowari.io/kotowari/board/vertex"
)

// Ko threat information.
type Ko struct {
	Vertex    vertex.Vertex
	OccuredBy Stone
}

// Initialize the Ko information.
func (ko *Ko) Init() {
	ko.Vertex = vertex.Outbound()
	ko.OccuredBy = Empty
}

// Set values to Ko information.
func (ko *Ko) Set(stone Stone, vx vertex.Vertex) {
	ko.Vertex = vx
	ko.OccuredBy = stone
}

// Copies Ko information.
func (ko *Ko) Copy() Ko {
	return Ko{ko.Vertex, ko.OccuredBy}
}
