package ko_test

import (
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	. "code.kotowari.io/kotowari/rule/ko"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Ko", func() {
	It("Init", func() {
		ko := Ko{Vertex{1, 1}, Black}
		ko.Init()
		Expect(ko).To(Equal(Ko{Outbound(), Empty}))
	})

	It("Set", func() {
		ko := Ko{Vertex{1, 1}, Black}
		ko.Set(White, Vertex{3, 5})
		Expect(ko).To(Equal(Ko{Vertex{3, 5}, White}))
	})

	It("Copy", func() {
		ko := Ko{Vertex{1, 1}, Black}
		copied := ko.Copy()
		Expect(copied).To(Equal(ko))
		copied.Set(White, Vertex{3, 5})
		Expect(copied).NotTo(Equal(ko))
	})
})
