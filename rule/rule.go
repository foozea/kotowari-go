/*
  Kotowari, a software for the game of Go
  Copyright (C) 2014 Tetsuo FUJII

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package rule

import (
	. "code.kotowari.io/kotowari/board/size"
	. "code.kotowari.io/kotowari/board/stone"
	. "code.kotowari.io/kotowari/board/vertex"
	. "code.kotowari.io/kotowari/rule/position"
)

// Structure for GameState variables.
type GameState struct {
	Size    BoardSize
	Current *Position
	Turn    Stone
	Komi    float64
	Times   TimeSettings
}

// Structure for time settings of state.
type TimeSettings struct {
	MainTime      int
	ByoYomiTime   int
	ByoYomiStones int
}

// Retruns default state of a rule.
func CreateDefaultGameState() GameState {
	komi := 0.0
	timeset := TimeSettings{60, 600, 25}
	return GameState{B9x9, nil, Empty, komi, timeset}
}

// Gets current position.
func (s *GameState) GetCurrentPosition() *Position {
	if s.Turn == Empty {
		pos := CreatePosition(s.Size)
		return &pos
	}
	return s.Current
}

// Gets the stone who plays this turn.
func (s *GameState) CurrentStoneIs() Stone {
	return s.Turn
}

// Makes a new move, puts the new position to the history.
// If the given move is invalid, returns false.
func (s *GameState) MakeMove(move Move) bool {
	pos := s.GetCurrentPosition()
	if pos.IsLegalMove(move) {
		next := pos.MakeMove(move)
		s.Current = &next
		s.Turn = move.Stone
		return true
	}
	return false
}

// Pass
func (s *GameState) Pass() {
	current := s.GetCurrentPosition()
	next := current.Copy()
	next.Ko.Init()
	s.Current = &next
	s.Turn = s.Turn.Opposite()
}

// Clears all history.
func (s *GameState) ClearHistory() {
	s.Current = nil
	s.Turn = Empty
}
